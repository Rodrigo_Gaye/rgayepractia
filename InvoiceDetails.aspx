﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InvoiceDetails.aspx.cs" Inherits="InvoiceDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Invoice details</title>
    <link href="Content/bootstrap.cosmo.min.css" rel="stylesheet" />
    <link href="Content/StyleSheet.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <br />
        <div id="mainContainer" class="container">
            <div class="shadowBox">
                <div class="page-container">
                    <div class="container">
                        <div class="jumbotron">
                            <h1>
                                <asp:Label ID="lbnInvoice" runat="server"></asp:Label>
                            </h1>
                        </div>
                        <div class="row">
                            <div class="col-lg-7 ">
                                <div class="table-responsive">

                                    <asp:GridView ID="gvInvoiceDetails" runat="server" Width="100%" CssClass="table table-striped table-bordered table-hover" AutoGenerateColumns="False" EmptyDataText="There are no data records to display.">
                                        <Columns>
                                            <asp:BoundField DataField="Name" HeaderText="Name" />
                                            <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                            <asp:BoundField DataField="SellPrice" HeaderText="SellPrice" />
                                            <asp:BoundField DataField="Total" HeaderText="Total" />
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <h4>
                <asp:Label ID="lbnTotalFactura" runat="server"></asp:Label>
            </h4>
        </div>
    </form>
</body>
</html>
