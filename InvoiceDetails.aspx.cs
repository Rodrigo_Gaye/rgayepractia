﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


public partial class InvoiceDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Params["parametroInvoice"] != null)
        {
            using (SqlConnection sqlCon = new SqlConnection("Data Source = QUIANE; Initial Catalog = D571D10D829540F46EAC535478A02676_PROYECTOS\\PROCESO DE SELECCIÓN\\PRUEBA NUEVA\\SOLUCION\\FACTURACION.WEB\\APP_DATA\\FACTURACIONDB.MDF; Integrated Security = True;"))
            {
                sqlCon.Open();
                string query = "select Name from Client where ClientId=" + Request.Params["parametroClient"];
                var command = new SqlCommand(query, sqlCon);
                lbnInvoice.Text = "Invoice " + Request.Params["parametroInvoice"] + " for " + command.ExecuteScalar().ToString();
                SqlDataAdapter sqlDa = new SqlDataAdapter("select p.Name, i.Amount, i.SellPrice, i.Total from Product p inner join InvoiceDetail i on p.ProductId=i.ProductId where InvoiceId=" + Request.Params["parametroInvoice"], sqlCon);
                System.Data.DataTable dtbl = new System.Data.DataTable();
                sqlDa.Fill(dtbl);
                gvInvoiceDetails.DataSource = dtbl;
                gvInvoiceDetails.DataBind();
                query = "select sum(Total) from InvoiceDetail where InvoiceId=" + Request.Params["parametroInvoice"];
                command = new SqlCommand(query, sqlCon);
                lbnTotalFactura.Text = "Invoice total: " + command.ExecuteScalar().ToString();
                sqlCon.Close();
            }
        }
        else
        {
            lbnInvoice.Text = "There is no data.";
        }

    }
}