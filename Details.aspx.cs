﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


public partial class Details : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["parametroClient"] != null)
        {

            using (SqlConnection sqlCon = new SqlConnection("Data Source = QUIANE; Initial Catalog = D571D10D829540F46EAC535478A02676_PROYECTOS\\PROCESO DE SELECCIÓN\\PRUEBA NUEVA\\SOLUCION\\FACTURACION.WEB\\APP_DATA\\FACTURACIONDB.MDF; Integrated Security = True;"))
            {
                sqlCon.Open();
                string query = "select Name from Client where ClientId=" + Request.Params["parametroClient"];
                var command = new SqlCommand(query, sqlCon);
                lblClient.Text = command.ExecuteScalar().ToString();

                SqlDataAdapter sqlDa = new SqlDataAdapter("select * from Client where ClientId=" + Request.Params["parametroClient"], sqlCon);
                System.Data.DataTable dtbl = new System.Data.DataTable();
                sqlDa.Fill(dtbl);
                gvClient.DataSource = dtbl;
                gvClient.DataBind();

                SqlDataAdapter sqlDa2 = new SqlDataAdapter("select * from Invoice where ClientId =" + Request.Params["parametroClient"], sqlCon);
                System.Data.DataTable dtbl1 = new System.Data.DataTable();
                sqlDa2.Fill(dtbl1);
                gvInvoice2.DataSource = dtbl1;
                gvInvoice2.DataBind();

                sqlCon.Close();
            }
        }
        else
        {
            lblClient.Text = "There is no data.";
        }
    }

    protected void lnkSelect_Click(object sender, EventArgs e)
    {
        string var = Request.Params["parametroClient"];
        int invoiceId = Convert.ToInt32((sender as LinkButton).CommandArgument);
        Response.Redirect("InvoiceDetails.aspx?parametroInvoice=" + invoiceId + "&parametroClient=" + var);

    }
}