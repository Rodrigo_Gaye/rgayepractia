﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


public partial class Clients : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        using (SqlConnection sqlCon = new SqlConnection("Data Source = QUIANE; Initial Catalog = D571D10D829540F46EAC535478A02676_PROYECTOS\\PROCESO DE SELECCIÓN\\PRUEBA NUEVA\\SOLUCION\\FACTURACION.WEB\\APP_DATA\\FACTURACIONDB.MDF; Integrated Security = True;"))
        {
            sqlCon.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter("select * from Client", sqlCon);
            System.Data.DataTable dtbl = new System.Data.DataTable();

            sqlDa.Fill(dtbl);
            gvClient.DataSource = dtbl;
            gvClient.DataBind();
            sqlCon.Close();
        }
    }

    protected void lnkSelect_Click(object sender, EventArgs e)
    {
        int clientId = Convert.ToInt32((sender as LinkButton).CommandArgument);
        Response.Redirect("Details.aspx?parametroClient=" + clientId);

    }
}