﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Details.aspx.cs" Inherits="Details" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Client detail and invoices</title>
    <link href="Content/bootstrap.cosmo.min.css" rel="stylesheet" />
    <link href="Content/StyleSheet.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <br />
        <div id="mainContainer" class="container">
            <div class="shadowBox">
                <div class="page-container">
                    <div class="container">
                        <div class="jumbotron">
                            <h1>
                                <asp:Label ID="lblClient" runat="server"></asp:Label>
                            </h1>
                        </div>
                        <div class="row">
                            <div class="col-lg-7 ">
                                <div class="table-responsive">

                                    <asp:GridView ID="gvClient" runat="server" Width="100%" CssClass="table table-striped table-bordered table-hover" AutoGenerateColumns="False" EmptyDataText="There are no data records to display.">
                                        <Columns>
                                            <asp:BoundField DataField="Address" HeaderText="Address" />
                                            <asp:BoundField DataField="BornDate" DataFormatString="{0:yyyy-MM-dd}" HeaderText="BornDate" />
                                            <asp:BoundField DataField="PhoneNumber" HeaderText="PhoneNumber" />
                                            <asp:BoundField DataField="Email" HeaderText="Email" />
                                        </Columns>
                                    </asp:GridView>

                                    <asp:GridView ID="gvInvoice2" runat="server" Width="100%" CssClass="table table-striped table-bordered table-hover" AutoGenerateColumns="False" EmptyDataText="There are no data records to display.">
                                        <Columns>
                                            <asp:BoundField DataField="InvoiceId" HeaderText="Invoice number" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="inkSelect" Text="Click here to see invoice details" runat="server" CommandArgument='<%# Eval("InvoiceId") %>' OnClick="lnkSelect_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
